import { Rocket } from '../app/models/Rocket';

export class LaunchesFilter {
    startDate : Date;
    endDate : Date;
    success : boolean;
    failure : boolean;
    rocket : Rocket;
}