import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SpacexApiProvider } from '../providers/spacex-api/spacex-api';
import { HttpClientModule } from '@angular/common/http';
import { LaunchListPage } from '../pages/launch-list/launch-list';
import { StatistiquePage } from '../pages/statistique/statistique';
import { AboutPage } from '../pages/about/about';
import { HistoriesPage } from '../pages/histories/histories';
import { LaunchDetailsPage } from '../pages/launch-details/launch-details';
import { VehiculesPage } from '../pages/vehicules/vehicules';
import { VehiculesPageDetailsPage } from '../pages/vehicules-page-details/vehicules-page-details';
import { HistoryDetailPage } from '../pages/history-detail/history-detail';
import { CapsuleListPage } from '../pages/capsule-list/capsule-list';
import { CapsuleDetailsPage } from '../pages/capsule-details/capsule-details';
import { LocalNotifications } from '@ionic-native/local-notifications';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LaunchListPage,
    StatistiquePage,
    AboutPage,
    HistoriesPage,
    HistoryDetailPage,
    LaunchDetailsPage,
    VehiculesPage,
    VehiculesPageDetailsPage,
    CapsuleListPage,
    CapsuleDetailsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LaunchListPage,
    StatistiquePage,
    AboutPage,
    HistoriesPage,
    HistoryDetailPage,
    LaunchDetailsPage,
    VehiculesPage,
    VehiculesPageDetailsPage,
    CapsuleListPage,
    CapsuleDetailsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SpacexApiProvider,
    LocalNotifications,
    InAppBrowser
  ]
})
export class AppModule {}
