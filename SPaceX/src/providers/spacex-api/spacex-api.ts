import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Launch } from '../../app/models/Launch';
import { Rocket } from '../../app/models/Rocket';
import { CompanyInfo } from '../../app/models/CompanyInfo';
import { CompanyHistory } from '../../app/models/CompanyHistory';
import { Capsule } from '../../app/models/Capsule';
import { LaunchPad } from '../../app/models/LaunchPad';
import { CapsuleDetail } from '../../app/models/CapsuleDetail';
import { CoreData } from '../../app/models/CoreData';
import { LaunchesFilter } from '../../app/LaunchesFilter';

/*
  Generated class for the SpacexApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SpacexApiProvider {
  baseUrl = 'https://api.spacexdata.com/v2'

  constructor(private http: HttpClient) {
  }

  getAllLaunches(options? : LaunchesFilter) : Observable<Launch[]> {
    var strOptions = "";
    if (options != null) {
      strOptions += '?';
      if (options.success && !options.failure) {
        strOptions += "launch_success=true&";
      } else if (!options.success && options.failure) {
        strOptions += "launch_success=false&";
      } 
      if ((options.startDate != null) && (options.endDate != null)) {
        var start = new Date(options.startDate);
        var end = new Date(options.endDate);
        strOptions += `start=${start.toUTCString()}&`;
        strOptions += `end=${end.toUTCString()}&`;
      }
      if ((options.rocket != null) && (options.rocket.id != null)) {
        strOptions += `rocket_id=${options.rocket.id}&`;
      }
    }
    const EndPointUrl = `${this.baseUrl}/launches/all${strOptions}`;
    var res = this.http.get<Launch[]>(EndPointUrl);
    if (res == null) {
      res = new Observable<Launch[]>();
    }
    return res;
  }

  getNextLaunch() : Observable<Launch> {
    const EndPointUrl = `${this.baseUrl}/launches/next`;
    return this.http.get<Launch>(EndPointUrl);
  }

  getAllLaunchesOfThisYear() : Observable<Launch[]> {
    const EndPointUrl = `${this.baseUrl}/launches/?launch_year=`+(new Date()).getFullYear();
    return this.http.get<Launch[]>(EndPointUrl);
  }

  getAllRockets(): Observable<Rocket[]> {
    const EndPointUrl = `${this.baseUrl}/rockets`;
    
    return this.http.get<Rocket[]>(EndPointUrl);
  }

  getCompanyInfo(): Observable<CompanyInfo> {
    const EndPointUrl = `${this.baseUrl}/info`;
    return this.http.get<CompanyInfo>(EndPointUrl);
  }

  getCompanyHistories(): Observable<CompanyHistory[]> {
    const EndPointUrl = `${this.baseUrl}/info/history`;
    return this.http.get<CompanyHistory[]>(EndPointUrl);
  }

  getRockets(): Observable<Rocket[]> {
    const EndPointUrl = `${this.baseUrl}/rockets`;
    return this.http.get<Rocket[]>(EndPointUrl);
  }

  getRocket(id: string): Observable<Rocket> {
    const EndPointUrl = `${this.baseUrl}/rockets/${id}`;
    return this.http.get<Rocket>(EndPointUrl);
  }

  getCapsules(): Observable<Capsule[]> {
    const EndPointUrl = `${this.baseUrl}/capsules`;
    return this.http.get<Capsule[]>(EndPointUrl);
  }

  getCapsule(id: string): Observable<Capsule> {
    const EndPointUrl = `${this.baseUrl}/capsules/${id}`;
    return this.http.get<Capsule>(EndPointUrl);
  }

  getLaunchPads(): Observable<LaunchPad[]> {
    const EndPointUrl = `${this.baseUrl}/launchpads`;
    return this.http.get<LaunchPad[]>(EndPointUrl);
  }

  getLaunchPad(id: string): Observable<LaunchPad> {
    const EndPointUrl = `${this.baseUrl}/launchpads/${id}`;
    return this.http.get<LaunchPad>(EndPointUrl);
  }

  getCapsuleDetails(): Observable<CapsuleDetail[]> {
    const EndPointUrl = `${this.baseUrl}/parts/caps`;
    return this.http.get<CapsuleDetail[]>(EndPointUrl);
  }

  getCapsuleDetail(id: string): Observable<CapsuleDetail> {
    const EndPointUrl = `${this.baseUrl}/parts/caps/${id}`;
    return this.http.get<CapsuleDetail>(EndPointUrl);
  }

  getCoreDatas(): Observable<CoreData[]> {
    const EndPointUrl = `${this.baseUrl}/parts/cores`;
    return this.http.get<CoreData[]>(EndPointUrl);
  }

  getCoreData(id: string): Observable<CoreData> {
    const EndPointUrl = `${this.baseUrl}/parts/cores/${id}`;
    return this.http.get<CoreData>(EndPointUrl);
  }

}
