import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController, Loading, LoadingController } from 'ionic-angular';
import { Launch } from '../../app/models/Launch';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SpacexApiProvider } from '../../providers/spacex-api/spacex-api';
import { VehiculesPageDetailsPage } from '../vehicules-page-details/vehicules-page-details';

/**
 * Generated class for the LaunchDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-launch-details',
  templateUrl: 'launch-details.html',
})
export class LaunchDetailsPage {

  launch: Launch;
  recycledParts: Array<string>;
  loading : Loading;
  constructor(public loadingCtrl: LoadingController,private navCtrl: NavController, public navParams: NavParams, private inAppBrowser: InAppBrowser, private spacexApi: SpacexApiProvider) {
    this.launch = this.navParams.data;
    this.loading = this.loadingCtrl.create({
      content: 'one moment please ...'
    });
    this.recycledParts = new Array<string>();
    this.spacexApi = spacexApi;
    // we get all the recycled parts
    for (let key in this.launch.reuse) {
      if (this.launch.reuse[key] == true) {
        this.recycledParts.push(key);
      }
    }
  }

  ionViewDidLoad() {
  }

  redirectToWeb(link: string) {
    this.inAppBrowser.create(link);
  }

  redirectToRocketDetails(rocketId: string) {
    this.loading.present();
    this.spacexApi.getRocket(rocketId).subscribe(data => {
      this.loading.dismiss();
      this.navCtrl.push(VehiculesPageDetailsPage, data);
    })
  }
}
