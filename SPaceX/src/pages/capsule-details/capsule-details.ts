import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Capsule } from '../../app/models/Capsule';

/**
 * Generated class for the CapsuleDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-capsule-details',
  templateUrl: 'capsule-details.html',
})
export class CapsuleDetailsPage {

  currentCapsule : Capsule
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.currentCapsule =  navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CapsuleDetailsPage');
  }

}
