import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SpacexApiProvider } from '../../providers/spacex-api/spacex-api';
import { CompanyHistory } from '../../app/models/CompanyHistory';
import { HistoryDetailPage } from '../history-detail/history-detail';

/**
 * Generated class for the HistoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-histories',
  templateUrl: 'histories.html',
})
export class HistoriesPage {

  histories: CompanyHistory[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private spacexApiProvider: SpacexApiProvider) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoriesPage');
    this.spacexApiProvider.getCompanyHistories().subscribe(data => {
      this.histories = data;
    })
  }

  goToHistoryDetail(history: CompanyHistory) {
    this.navCtrl.push(HistoryDetailPage, history);
  }
}
