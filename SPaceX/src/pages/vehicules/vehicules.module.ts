import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehiculesPage } from './vehicules';

@NgModule({
  declarations: [
    VehiculesPage,
  ],
  imports: [
    IonicPageModule.forChild(VehiculesPage),
  ],
})
export class VehiculesPageModule {}
