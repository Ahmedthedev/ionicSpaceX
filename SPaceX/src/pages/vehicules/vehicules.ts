import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading} from 'ionic-angular';
import { SpacexApiProvider } from '../../providers/spacex-api/spacex-api';
import { Rocket } from '../../app/models/Rocket';
import { VehiculesPageDetailsPage } from '../vehicules-page-details/vehicules-page-details';
/**
 * Generated class for the VehiculesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vehicules',
  templateUrl: 'vehicules.html',
})
export class VehiculesPage {
  searchRocket: any;
  items : any;
  rockets : Rocket[];
  loading : Loading;
  displayedRocket : Rocket[];
  constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, private spacexApi: SpacexApiProvider) {
     this.loading = this.loadingCtrl.create({
      content: 'one moment please ...'
    });
    this.loading.present();
  }

  goToVehiculesPagesDetails(rocket:Rocket) {
    this.navCtrl.push(VehiculesPageDetailsPage, rocket);
  }

  onInput(event:any) {
    this.displayedRocket = this.rockets.filter(rocket=> rocket.name.toLowerCase().startsWith(this.searchRocket.toLowerCase()));
    if(this.searchRocket === "") {
      this.displayedRocket = this.rockets;
    }
  }
  
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad VehiculesPage');
    this.spacexApi.getAllRockets().subscribe(data => {
      this.loading.dismiss();
      this.rockets = data;
      this.displayedRocket = this.rockets
    })
  }

}
