import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehiculesPageDetailsPage } from './vehicules-page-details';

@NgModule({
  declarations: [
    VehiculesPageDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(VehiculesPageDetailsPage),
  ],
})
export class VehiculesPageDetailsPageModule {}
