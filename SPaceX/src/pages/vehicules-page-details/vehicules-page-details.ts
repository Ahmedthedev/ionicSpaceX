import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Rocket } from '../../app/models/Rocket';
/**
 * Generated class for the VehiculesPageDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vehicules-page-details',
  templateUrl: 'vehicules-page-details.html',
})
export class VehiculesPageDetailsPage {

  currentRocket : Rocket
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.currentRocket =  navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VehiculesPageDetailsPage');
  }
}
