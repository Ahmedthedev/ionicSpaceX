import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SpacexApiProvider } from '../../providers/spacex-api/spacex-api';
import { CompanyInfo } from '../../app/models/CompanyInfo';
import { HistoriesPage } from '../histories/histories';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  companyInfo : CompanyInfo;

  constructor(public navCtrl: NavController, public navParams: NavParams, private spacexApiProvider: SpacexApiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
    this.spacexApiProvider.getCompanyInfo().subscribe(data => this.companyInfo = data);
  }

  goToHistories() {
    this.navCtrl.push(HistoriesPage);
  }

}
