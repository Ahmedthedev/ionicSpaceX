import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { Launch } from '../../app/models/Launch';
import { SpacexApiProvider } from '../../providers/spacex-api/spacex-api';

/**
 * Generated class for the StatistiquePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-statistique',
  templateUrl: 'statistique.html',
})
export class StatistiquePage {

  @ViewChild('barCanvas') barCanvas;
  @ViewChild('doughnutCanvas') doughnutCanvas;
  @ViewChild('lineCanvas') lineCanvas;

  barChart: any;
  doughnutChart: any;
  lineChart: any;
  launches: Launch[];

  constructor(public navCtrl: NavController, private spacexApi: SpacexApiProvider) {
    this.spacexApi.getAllLaunches().subscribe(data => {
      this.launches = data;

      let dictionnaryOfYears : { [id: string] :  number; } = {};
      let labelsList:string[] = new Array()
      let datasList:string[] = new Array()
      let i = 0

      this.launches.forEach(element => {
        dictionnaryOfYears[element.launch_year] = 0 
      });
      this.launches.forEach(element => {
        dictionnaryOfYears[element.launch_year]++
      });
      for(let k in dictionnaryOfYears) {
        labelsList[i] = k
        datasList[i] = (String(dictionnaryOfYears[k]))
        i++
      }

     this.lineChart = new Chart(this.lineCanvas.nativeElement, {
        type: 'line',
        data: {
            labels: labelsList,
            datasets: [
                {
                    label: "Number of launches",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: datasList,
                    spanGaps: false,
                }
            ]
        }

    });
    })
  }

  ionViewDidLoad() {
      
  }

}
