import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController, Loading, LoadingController } from 'ionic-angular';
import { Capsule } from '../../app/models/Capsule';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SpacexApiProvider } from '../../providers/spacex-api/spacex-api';
import { CapsuleDetailsPage } from '../capsule-details/capsule-details';
/**
 * Generated class for the CapsuleListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-capsule-list',
  templateUrl: 'capsule-list.html',
})
export class CapsuleListPage {
  searchCapsule: any;
  capsules: Capsule[];
  loading : Loading;
  displayedCapsule : Capsule[];
  constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, private spacexApi: SpacexApiProvider) {
     this.loading = this.loadingCtrl.create({
      content: 'one moment please ...'
    });
    this.loading.present();
  }

  goToCapsuleDetailsPage(capsule:Capsule) {
    this.navCtrl.push(CapsuleDetailsPage, capsule);
  }

  onInput(event:any) {
    this.displayedCapsule = this.capsules.filter(capsule => capsule.name.toLowerCase().startsWith(this.searchCapsule.toLowerCase()));
    if(this.searchCapsule === "") {
      this.displayedCapsule = this.capsules;
    }
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad VehiculesPage');
    this.spacexApi.getCapsules().subscribe(data => {
      this.loading.dismiss();
      this.capsules = data;
      this.displayedCapsule = data;
    })
  }

}
