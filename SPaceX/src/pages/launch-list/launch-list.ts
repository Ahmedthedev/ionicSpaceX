import { Component } from '@angular/core';
import { IonicPage, NavController, Loading, LoadingController } from 'ionic-angular';
import { SpacexApiProvider } from '../../providers/spacex-api/spacex-api';
import { Launch } from '../../app/models/Launch';
import { Rocket } from '../../app/models/Rocket';
import { LaunchDetailsPage } from '../launch-details/launch-details';
import { LaunchesFilter } from '../../app/LaunchesFilter';

/**
 * Generated class for the LaunchListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-launch-list',
  templateUrl: 'launch-list.html',
})
export class LaunchListPage {

  launches: Launch[];
  loading: Loading;
  rockets: Rocket[];
  launchesForm: LaunchesFilter;

  constructor(private navCtrl: NavController, private spacexApi: SpacexApiProvider, public loadingCtrl: LoadingController) {
    this.loading = this.loadingCtrl.create({
      content: 'one moment please ...'
    });
    this.loading.present();
    this.launchesForm = new LaunchesFilter();
    this.launchesForm.success = true;
    this.launchesForm.failure = true;
    this.spacexApi.getAllRockets().subscribe(data => {
      this.rockets = data;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LaunchListPage');
    
    this.spacexApi.getAllLaunches().subscribe(data => {
      this.loading.dismiss();
      this.refreshData(data);
      this.launchesForm.startDate = this.launches[this.launches.length - 1].launch_date_utc;
      this.launchesForm.endDate = this.launches[0].launch_date_utc;
    })

  }

  goToDetails(launch: Launch) {
    this.navCtrl.push(LaunchDetailsPage, launch);
  }

  formChanged() {
    this.spacexApi.getAllLaunches(this.launchesForm).subscribe(data => {
      this.refreshData(data);
    });
  }

  refreshData(data) {
    this.launches = data;
    // order from newer to older.
    this.launches.sort((a, b) => {
      if (a.launch_date_unix < b.launch_date_unix) {
        return 1;
      } else if (a.launch_date_unix > b.launch_date_unix) {
        return -1;
      }
      return 0;
    })
  }

}
