import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SpacexApiProvider } from '../../providers/spacex-api/spacex-api';
import { Launch } from '../../app/models/Launch';
import { LocalNotifications } from '@ionic-native/local-notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  launch: Launch;
  nextLaunchTimeStr: Date;
  timer: number;
  launchCount = 0;
  unixTime = 0;
  isNotificationSet = false;

  constructor(public navCtrl: NavController, private spacexApiProvider: SpacexApiProvider, private localNotifications: LocalNotifications) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.spacexApiProvider.getAllLaunchesOfThisYear().subscribe(data => {
      this.launchCount = data.length;
    });
    this.spacexApiProvider.getNextLaunch().subscribe(data => {
      this.launch = data;
      this.timer = setInterval(x => 
        {
          
            this.getNextLaunchTimeStr();
        }, 1000);
      })

      
  }

  getTimeStringFromUnixTime(unixTimeStamp: number) {
    // Create a new JavaScript Date object based on the timestamp
    // multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date = new Date(unixTimeStamp*1000);

    return date;
  }

  getNextLaunchTimeStr() {
    let val =  this.launch.launch_date_unix - Date.now();
    this.unixTime = val;
    this.nextLaunchTimeStr = new Date(Math.round(this.getTimeStringFromUnixTime(this.unixTime).getTime()/1000));

    if(!this.isNotificationSet) {
      // Schedule delayed notification
      this.localNotifications.schedule({
        text: 'Launched !',
        trigger: {at: new Date(this.launch.launch_date_unix)},
        led: 'FF0000',
        sound: null
      });
      this.isNotificationSet = true;
    }
  
    return this.nextLaunchTimeStr;
  }

  

}
